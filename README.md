# MyAppInfo
[![Gem Version](https://badge.fury.io/rb/my_app_info.png)](https://badge.fury.io/rb/my_app_info)

A small Gem that allows you to generate a copyright notice, application name and version number for your app.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'my_app_info'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install my_app_info

## Usage

I like to create these three methods that are in the application_helper.rb file in my rails apps. 

```ruby
  def set_copyright
    MyAppInfo::Renderer.app_copyright 'ACME Widgets', 'All Rights Reserved'
  end

  def set_version
    MyAppInfo::Renderer.app_version 'ACME Widgets Blog', '1.5a'
  end

  def set_policy
    MyAppInfo::Renderer.app_privacy 'Our Privacy Policy', 'https://www.acme-widgets.com/privacy-policy.html'
  end
```
Then call the rendered methods at the bottom of the page in your application HTML template file.

```ruby
<%= set_version %> | <%= set_policy %> | <%= set_copyright %>
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/mchilson/my_app_info. Contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT). Enjoy!

