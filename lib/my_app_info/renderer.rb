module MyAppInfo
  class Renderer
    def self.app_copyright name, msg
      "Copyright &copy; #{Time.now.year} #{name}, #{msg}".html_safe
    end
    def self.app_version app_name, ver
      "#{app_name}, Version #{ver}".html_safe
    end
    def self.app_privacy privacy_text, privacy_url
      "<a href='#{privacy_url}'>#{privacy_text}</a>".html_safe
    end
  end
end
