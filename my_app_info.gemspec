# coding: utf-8
# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'my_app_info/version'

Gem::Specification.new do |spec|
  spec.name          = "my_app_info"
  spec.version       = MyAppInfo::VERSION
  spec.authors       = ["Mike Chilson"]
  spec.email         = ["emailme@mikechilson.com"]
  
  spec.summary       = %q{A Rails Gem that adds copyright, a privacy policy link, and version info to your Rails application.}
  spec.description   = %q{A small Rails Gem that allows you to generate a copyright notice, a privacy policy link, application name and version number for your Rails application.}
  spec.homepage      = "https://gitlab.com/mchilson/my_app_info"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
end
